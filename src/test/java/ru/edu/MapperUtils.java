package ru.edu;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

public class MapperUtils {
    @SuppressWarnings("unchecked")
    public static <T> T readJson(String filename, Class<T> clazz) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(filename), clazz);
    }
    @SuppressWarnings("unchecked")
    public static <T> T readXml(String filename, Class<T> clazz) throws IOException {
        XmlMapper mapper = new XmlMapper();
        return mapper.readValue(new File(filename), clazz);
    }
    @SuppressWarnings("unchecked")
    public static <T> T readSerialized(String filename, Class<T> clazz) throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = null;
        try {
            FileInputStream fileInputStream = new FileInputStream(filename);
            objectInputStream = new ObjectInputStream(fileInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return(T) objectInputStream.readObject();
    }
    @SuppressWarnings("unchecked")
    public static void writeSerialized(String filename, Object obj) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(filename);
        ObjectOutputStream objectOutputStream =
                new ObjectOutputStream(outputStream);

        objectOutputStream.writeObject(obj);
        objectOutputStream.close();
    }
}
