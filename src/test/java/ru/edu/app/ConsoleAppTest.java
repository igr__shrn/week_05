package ru.edu.app;

import org.junit.Before;
import org.junit.Test;

public class ConsoleAppTest {
    private final String INPUT = "-input=./input/cd_catalog.xml";
    private final String OUTPUT_XML = "-output=./output/console_artist_by_country.xml";
    private final String OUTPUT_JSON = "-output=./output/console_artist_by_country.json";
    private final String OUTPUT_JAVA = "-output=./output/console_artist_by_country.serialized";

    String XML = "-format=XML";
    String JSON = "-format=JSON";
    String SERIALIZED = "-format=JAVA";

    String[] argsXml;
    String[] argsJson;
    String[] argsJava;

    @Before
    public void setup(){
        argsXml = new String[]{INPUT, OUTPUT_XML, XML};
        argsJson = new String[]{INPUT, OUTPUT_JSON, JSON};
        argsJava = new String[]{INPUT, OUTPUT_JAVA, SERIALIZED};
    }

    @Test
    public void testXml(){
        ConsoleApp.main(argsXml);
    }

    @Test
    public void testJson(){
        ConsoleApp.main(argsJson);
    }

    @Test
    public void testJava(){
        ConsoleApp.main(argsJava);
    }
    @Test
    public void testHelp(){
        ConsoleApp.main(new String[]{"-help"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIllegalFormat(){
        ConsoleApp.main(new String[]{INPUT, OUTPUT_JAVA, "IllegalFormat"});
    }

}