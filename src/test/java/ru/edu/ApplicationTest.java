package ru.edu;

import org.junit.Test;

public class ApplicationTest {
    Application app;

    String INPUT_PATH = "./input/cd_catalog.xml";
    String OUTPUT_PATH_XML = "./output/console_artist_by_country.xml";
    String OUTPUT_PATH_JSON = "./output/console_artist_by_country.json";
    String OUTPUT_PATH_SERIALIZED = "./output/console_artist_by_country.serialized";

    String XML = "XML";
    String JSON = "JSON";
    String SERIALIZED = "JAVA";

    @Test
    public void testXml(){
        app = new Application(INPUT_PATH, OUTPUT_PATH_XML, XML);
    }

    @Test
    public void testJson(){
        app = new Application(INPUT_PATH, OUTPUT_PATH_JSON, JSON);
    }

    @Test
    public void testSerialize(){
        app = new Application(INPUT_PATH, OUTPUT_PATH_SERIALIZED, SERIALIZED);
    }

}