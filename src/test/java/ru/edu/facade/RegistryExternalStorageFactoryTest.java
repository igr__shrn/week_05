package ru.edu.facade;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RegistryExternalStorageFactoryTest {
    private final String TEST = "TEST";
    private final String XML = "XML";
    private final String JSON = "JSON";
    private final String SERIALIZED = "JAVA";

    RegistryExternalStorage xmlStorage;
    RegistryExternalStorage jsonStorage;
    RegistryExternalStorage serializedStorage;

    @Before
    public void setup() {
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFactoryExternalStorageIllegal() {
        RegistryExternalStorageFactory.factoryExternalStorage(TEST);
    }

    @Test
    public void testFactoryExternalStorageXml() {
        xmlStorage = RegistryExternalStorageFactory.factoryExternalStorage(XML);
        assertEquals(XML, xmlStorage.getType());
    }

    @Test
    public void testFactoryExternalStorageJson() {
        jsonStorage = RegistryExternalStorageFactory.factoryExternalStorage(JSON);
        assertEquals(JSON, jsonStorage.getType());
    }

    @Test
    public void testFactoryExternalStorageSerialized() {
        serializedStorage = RegistryExternalStorageFactory.factoryExternalStorage(SERIALIZED);
        assertEquals(SERIALIZED, serializedStorage.getType());
    }

}