package ru.edu.facade;

import org.junit.Before;
import org.junit.Test;

public class RegistryExternalStorageSerializedTest {
    RegistryExternalStorageSerialized serializedStorage;

    @Before
    public void setup() {
        serializedStorage = new RegistryExternalStorageSerialized();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetupNull() {
        serializedStorage.setup(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetupNotExists() {
        serializedStorage.setup("1.xml");
    }
}