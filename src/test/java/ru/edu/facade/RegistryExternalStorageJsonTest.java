package ru.edu.facade;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.MockitoAnnotations.openMocks;

public class RegistryExternalStorageJsonTest {

    RegistryExternalStorageJson jsonStorage;

    @Before
    public void setup(){
        openMocks(this);
        jsonStorage = new RegistryExternalStorageJson();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetupNull(){
        jsonStorage.setup(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetupNotExists(){
        jsonStorage.setup("1.xml");
    }

}