package ru.edu.facade;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.MockitoAnnotations.openMocks;

public class SourceReaderXmlTest {
    SourceReaderXml reader;

    @Before
    public void setup() {
        openMocks(this);
        reader = new SourceReaderXml();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetupNull() {
        reader.setup(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetupNotExists() {
        reader.setup("1.xml");
    }

}