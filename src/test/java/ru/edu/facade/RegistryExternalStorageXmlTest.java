package ru.edu.facade;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.MockitoAnnotations.openMocks;

public class RegistryExternalStorageXmlTest {
    RegistryExternalStorageXml xmlStorage;

    @Before
    public void setup() {
        openMocks(this);
        xmlStorage = new RegistryExternalStorageXml();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetupNull() {
        xmlStorage.setup(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetupNotExists() {
        xmlStorage.setup("1.xml");
    }

}