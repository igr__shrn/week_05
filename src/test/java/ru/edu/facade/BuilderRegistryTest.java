package ru.edu.facade;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.model.CD;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class BuilderRegistryTest {

    private final String ALBUM_NAME = "ALBUM";
    private final String COUNTRY1 = "RU";

    @Mock
    Catalog catalog;

    @Mock
    CD cd;

    BuilderRegistry builder;
    Registry registry;
    List<CD> listCd;

    @Before
    public void setup() {
        openMocks(this);

        listCd = new ArrayList<>();
        listCd.add(createCd(COUNTRY1, ALBUM_NAME));

        builder = new BuilderRegistry();
        when(catalog.getCd()).thenReturn(listCd);
    }

    private CD createCd(String countryName, String title) {
        when(cd.getCountry()).thenReturn(countryName);
        when(cd.getTitle()).thenReturn(title);

        return cd;
    }

    @Test
    public void testBuild() {
        registry = builder.build(catalog);
        assertEquals(1, registry.getCountryCount());
        assertEquals(COUNTRY1, registry.getCountries().get(0).getName());
        assertEquals(ALBUM_NAME, registry.getCountries().get(0).getArtists().get(0).getAlbums().get(0).getName());
    }

}