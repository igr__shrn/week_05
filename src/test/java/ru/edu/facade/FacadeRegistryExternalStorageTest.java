package ru.edu.facade;

import org.junit.Test;

public class FacadeRegistryExternalStorageTest {

    FacadeRegistryExternalStorage storage;

    String INPUT_PATH = "./input/cd_catalog.xml";
    String OUTPUT_PATH_XML = "./output/artist_by_country.xml";
    String OUTPUT_PATH_JSON = "./output/artist_by_country.json";
    String OUTPUT_PATH_SERIALIZED = "./output/artist_by_country.serialized";

    String XML = "XML";
    String JSON = "JSON";
    String SERIALIZED = "JAVA";

    @Test
    public void testXml(){
        storage = new FacadeRegistryExternalStorage(INPUT_PATH, OUTPUT_PATH_XML, XML);
        storage.go();
    }

    @Test
    public void testJson(){
        storage = new FacadeRegistryExternalStorage(INPUT_PATH, OUTPUT_PATH_JSON, JSON);
        storage.go();
    }

    @Test
    public void testSerialize(){
        storage = new FacadeRegistryExternalStorage(INPUT_PATH, OUTPUT_PATH_SERIALIZED, SERIALIZED);
        storage.go();
    }

}