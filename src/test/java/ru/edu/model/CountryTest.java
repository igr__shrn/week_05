package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.MapperUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class CountryTest {
    private final String INPUT_PATH_XML = "./src/test/resources/model/country/country.xml";
    private final String INPUT_PATH_JSON = "./src/test/resources/model/country/country.json";
    private final String INPUT_PATH_SERIALIZED = "./src/test/resources/model/country/country.serialized";

    private final String COUNTRY_NAME = "USA";
    private final String ARTIST_NAME = "Bob Dylan";
    private final String ALBUM_NAME = "Empire Burlesque";
    private final int ALBUM_YEAR = 1985;
    private final double ALBUM_PRICE = 10.9;

    Country countryXml;
    Country countryJson;
    Country countrySerialized;

    @Mock
    Artist artist;

    @Mock
    Album album;

    private List<Artist> artists;
    private List<Album> albums;

    @Before
    public void setup() throws IOException, ClassNotFoundException {
        openMocks(this);

        countryXml = MapperUtils.readXml(INPUT_PATH_XML, Country.class);
        countryJson = MapperUtils.readJson(INPUT_PATH_JSON, Country.class);
        MapperUtils.writeSerialized(INPUT_PATH_SERIALIZED, countryJson);
        countrySerialized = MapperUtils.readSerialized(INPUT_PATH_SERIALIZED, Country.class);

        albums = new ArrayList<>();
        albums.add(createMockAlbum(ALBUM_NAME, ALBUM_YEAR, ALBUM_PRICE));

        artists = new ArrayList<>();
        artists.add(createMockArtist(ARTIST_NAME));

    }

    private Artist createMockArtist(String artistName) {
        when(artist.getName()).thenReturn(artistName);
        when(artist.getAlbums()).thenReturn(albums);

        return artist;
    }

    private Album createMockAlbum(String albumName, long albumYear, double albumPrice) {
        when(album.getName()).thenReturn(albumName);
        when(album.getYear()).thenReturn(albumYear);
        when(album.getPrice()).thenReturn(albumPrice);

        return album;
    }

    private Album getAlbum(Country country, int artistId, int albumId) {
        return getArtist(country, artistId).getAlbums().get(albumId);
    }

    private Artist getArtist(Country country, int artistId) {
        return country.getArtists().get(artistId);
    }

    @Test
    public void testXmlNameCountry() {
        assertEquals(COUNTRY_NAME, countryXml.getName());
    }

    @Test
    public void testXmlArtists() {
        assertEquals(ARTIST_NAME, getArtist(countryXml, 0).getName());
    }

    @Test
    public void testXmlAlbums() {
        assertEquals(ALBUM_NAME, getAlbum(countryXml, 0, 0).getName());
        assertEquals(ALBUM_YEAR, getAlbum(countryXml, 0, 0).getYear());
        assertThat(ALBUM_PRICE, equalTo(getAlbum(countryXml, 0, 0).getPrice()));
    }

    @Test
    public void testJsonNameCountry() {
        assertEquals(COUNTRY_NAME, countryJson.getName());
    }

    @Test
    public void testJsonArtists() {
        assertEquals(ARTIST_NAME, getArtist(countryJson, 0).getName());
    }

    @Test
    public void testJsonAlbums() {
        assertEquals(ALBUM_NAME, getAlbum(countryJson, 0, 0).getName());
        assertEquals(ALBUM_YEAR, getAlbum(countryJson, 0, 0).getYear());
        assertThat(ALBUM_PRICE, equalTo(getAlbum(countryJson, 0, 0).getPrice()));
    }

    @Test
    public void testSerializedNameCountry() {
        assertEquals(COUNTRY_NAME, countrySerialized.getName());
    }

    @Test
    public void testSerializedArtists() {
        assertEquals(ARTIST_NAME, getArtist(countrySerialized, 0).getName());
    }

    @Test
    public void testSerializedAlbums() {
        assertEquals(ALBUM_NAME, getAlbum(countrySerialized, 0, 0).getName());
        assertEquals(ALBUM_YEAR, getAlbum(countrySerialized, 0, 0).getYear());
        assertThat(ALBUM_PRICE, equalTo(getAlbum(countrySerialized, 0, 0).getPrice()));
    }

}