package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.MapperUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class ArtistTest {
    private final String INPUT_PATH_XML = "./src/test/resources/model/artist/artist.xml";
    private final String INPUT_PATH_JSON = "./src/test/resources/model/artist/artist.json";
    private final String INPUT_PATH_SERIALIZED = "./src/test/resources/model/artist/artist.serialized";

    private final String ARTIST_NAME = "Bob Dylan";
    private final String ALBUM_NAME = "Empire Burlesque";
    private final long ALBUM_YEAR = 1985;
    private final double ALBUM_PRICE = 10.9;

    Artist artistXml;
    Artist artistJson;
    Artist artistSerialized;

    private List<Album> albums;

    @Mock
    private Album album;

    @Before
    public void setup() throws IOException, ClassNotFoundException {
        openMocks(this);

        artistXml = MapperUtils.readXml(INPUT_PATH_XML, Artist.class);
        artistJson = MapperUtils.readJson(INPUT_PATH_JSON, Artist.class);
        MapperUtils.writeSerialized(INPUT_PATH_SERIALIZED, artistJson);
        artistSerialized = MapperUtils.readSerialized(INPUT_PATH_SERIALIZED, Artist.class);

        albums = new ArrayList<>();
        albums.add(createMockAlbum(ALBUM_NAME, ALBUM_YEAR, ALBUM_PRICE));
    }

    private Album createMockAlbum(String albumName, long albumYear, double albumPrice) {
        when(album.getName()).thenReturn(albumName);
        when(album.getYear()).thenReturn(albumYear);
        when(album.getPrice()).thenReturn(albumPrice);

        return album;
    }

    private Album getAlbum(Artist artist, int albumId) {
        return artist.getAlbums().get(albumId);
    }

    @Test
    public void testXmlNameCountry() {
        assertEquals(ARTIST_NAME, artistXml.getName());
    }

    @Test
    public void testXmlAlbums() {
        assertEquals(ALBUM_NAME, getAlbum(artistXml, 0).getName());
        assertEquals(ALBUM_YEAR, getAlbum(artistXml, 0).getYear());
        assertThat(ALBUM_PRICE, equalTo(getAlbum(artistXml, 0).getPrice()));
    }

    @Test
    public void testJsonNameCountry() {
        assertEquals(ARTIST_NAME, artistJson.getName());
    }

    @Test
    public void testJsonAlbums() {
        assertEquals(ALBUM_NAME, getAlbum(artistJson, 0).getName());
        assertEquals(ALBUM_YEAR, getAlbum(artistJson, 0).getYear());
        assertThat(ALBUM_PRICE, equalTo(getAlbum(artistJson, 0).getPrice()));
    }

    @Test
    public void testSerializedNameCountry() {
        assertEquals(ARTIST_NAME, artistSerialized.getName());
    }

    @Test
    public void testSerializedAlbums() {
        assertEquals(ALBUM_NAME, getAlbum(artistSerialized, 0).getName());
        assertEquals(ALBUM_YEAR, getAlbum(artistSerialized, 0).getYear());
        assertThat(ALBUM_PRICE, equalTo(getAlbum(artistSerialized, 0).getPrice()));
    }


}