package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import ru.edu.MapperUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class RegistryTest {

    private final String INPUT_PATH_XML = "./src/test/resources/model/registry/registry.xml";
    private final String INPUT_PATH_JSON = "./src/test/resources/model/registry/registry.json";
    private final String INPUT_PATH_SERIALIZED = "./src/test/resources/model/registry/registry.serialized";

    private final int COUNTRY_COUNT = 1;
    private final String COUNTRY_NAME = "USA";
    private final String ARTIST_NAME = "Bob Dylan";
    private final String ALBUM_NAME = "Empire Burlesque";
    private final int ALBUM_YEAR = 1985;
    private final double ALBUM_PRICE = 10.9;

    Registry registryXml;
    Registry registryJson;
    Registry registrySerialized;

    @Mock
    Country country;

    @Mock
    Artist artist;

    @Mock
    Album album;

    private List<Country> countries;
    private List<Artist> artists;
    private List<Album> albums;

    @Before
    public void setup() throws IOException, ClassNotFoundException {
        openMocks(this);

        registryXml = MapperUtils.readXml(INPUT_PATH_XML, Registry.class);
        registryJson = MapperUtils.readJson(INPUT_PATH_JSON, Registry.class);
        MapperUtils.writeSerialized(INPUT_PATH_SERIALIZED, registryJson);
        registrySerialized = MapperUtils.readSerialized(INPUT_PATH_SERIALIZED, Registry.class);

        albums = new ArrayList<>();
        albums.add(createMockAlbum(ALBUM_NAME, ALBUM_YEAR, ALBUM_PRICE));
        albums.add(createMockAlbum(ALBUM_NAME, ALBUM_YEAR, ALBUM_PRICE));

        artists = new ArrayList<>();
        artists.add(createMockArtist(ARTIST_NAME));

        countries = new ArrayList<>();
        countries.add(createCountry(COUNTRY_NAME));
    }

    private Country createCountry(String countryName) {
        when(country.getName()).thenReturn(countryName);
        when(country.getArtists()).thenReturn(artists);

        return country;
    }

    private Artist createMockArtist(String artistName) {
        when(artist.getName()).thenReturn(artistName);
        when(artist.getAlbums()).thenReturn(albums);

        return artist;
    }

    private Album createMockAlbum(String albumName, long albumYear, double albumPrice) {
        when(album.getName()).thenReturn(albumName);
        when(album.getYear()).thenReturn(albumYear);
        when(album.getPrice()).thenReturn(albumPrice);

        return album;
    }

    private Country getCountry(Registry registry, int countryId) {
        return registry.getCountries().get(countryId);
    }

    private Artist getArtist(Registry registry, int countryId, int artistId) {
        return getCountry(registry, countryId).getArtists().get(artistId);
    }

    private Album getAlbum(Registry registry, int countryId, int artistId, int albumId) {
        return getArtist(registry, countryId, artistId).getAlbums().get(albumId);
    }

    @Test
    public void testXmlCountryCount() {
        assertEquals(COUNTRY_COUNT, registryXml.getCountryCount());
    }

    @Test
    public void testXmlCountryName() {
        assertEquals(COUNTRY_NAME, getCountry(registryXml, 0).getName());
    }

    @Test
    public void testXmlArtist() {
        assertEquals(ARTIST_NAME, getArtist(registryXml, 0, 0).getName());
    }

    @Test
    public void testXmlAlbums() {
        assertEquals(ALBUM_NAME, getAlbum(registryXml, 0, 0, 0).getName());
        assertEquals(ALBUM_YEAR, getAlbum(registryXml, 0, 0, 0).getYear());
        assertThat(ALBUM_PRICE, equalTo(getAlbum(registryXml, 0, 0, 0).getPrice()));
    }

    @Test
    public void testJsonCountryCount() {
        assertEquals(COUNTRY_COUNT, registryJson.getCountryCount());
    }

    @Test
    public void testJsonCountryName() {
        assertEquals(COUNTRY_NAME, getCountry(registryJson, 0).getName());
    }

    @Test
    public void testJsonArtist() {
        assertEquals(ARTIST_NAME, getArtist(registryJson, 0, 0).getName());
    }

    @Test
    public void testJsonAlbums() {
        assertEquals(ALBUM_NAME, getAlbum(registryJson, 0, 0, 0).getName());
        assertEquals(ALBUM_YEAR, getAlbum(registryJson, 0, 0, 0).getYear());
        assertThat(ALBUM_PRICE, equalTo(getAlbum(registryJson, 0, 0, 0).getPrice()));
    }

    @Test
    public void testSerializedCountryCount() {
        assertEquals(COUNTRY_COUNT, registryJson.getCountryCount());
    }

    @Test
    public void testSerializedCountryName() {
        assertEquals(COUNTRY_NAME, getCountry(registrySerialized, 0).getName());
    }

    @Test
    public void testSerializedArtist() {
        assertEquals(ARTIST_NAME, getArtist(registrySerialized, 0, 0).getName());
    }

    @Test
    public void testSerializedAlbums() {
        assertEquals(ALBUM_NAME, getAlbum(registrySerialized, 0, 0, 0).getName());
        assertEquals(ALBUM_YEAR, getAlbum(registrySerialized, 0, 0, 0).getYear());
        assertThat(ALBUM_PRICE, equalTo(getAlbum(registrySerialized, 0, 0, 0).getPrice()));
    }


}