package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import ru.edu.MapperUtils;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class AlbumTest {
    private final String INPUT_PATH_XML = "./src/test/resources/model/album/album.xml";
    private final String INPUT_PATH_JSON = "./src/test/resources/model/album/album.json";
    private final String INPUT_PATH_SERIALIZED = "./src/test/resources/model/album/album.serialized";

    private final String NAME = "Empire Burlesque";
    private final int YEAR = 1985;
    private final double PRICE = 10.9;

    Album albumXml;
    Album albumJson;
    Album albumSerialized;

    @Before
    public void setup() throws IOException, ClassNotFoundException {
        albumXml = MapperUtils.readXml(INPUT_PATH_XML, Album.class);
        albumJson = MapperUtils.readJson(INPUT_PATH_JSON, Album.class);
        MapperUtils.writeSerialized(INPUT_PATH_SERIALIZED, albumJson);
        albumSerialized = MapperUtils.readSerialized(INPUT_PATH_SERIALIZED, Album.class);
    }

    @Test
    public void testXmlName() {
        assertEquals(NAME, albumXml.getName());
    }

    @Test
    public void testXmlYear() {
        assertEquals(YEAR, albumXml.getYear());
    }

    @Test
    public void testXmlPrice() {
        assertThat(PRICE, equalTo(albumXml.getPrice()));
    }

    @Test
    public void testJsonName() {
        assertEquals(NAME, albumJson.getName());
    }

    @Test
    public void testJsonYear() {
        assertEquals(YEAR, albumJson.getYear());
    }

    @Test
    public void testJsonPrice() {
        assertThat(PRICE, equalTo(albumJson.getPrice()));
    }

    @Test
    public void testSerializedName() {
        assertEquals(NAME, albumSerialized.getName());
    }

    @Test
    public void testSerializedYear() {
        assertEquals(YEAR, albumSerialized.getYear());
    }

    @Test
    public void testSerializedPrice() {
        assertThat(PRICE, equalTo(albumSerialized.getPrice()));
    }
}