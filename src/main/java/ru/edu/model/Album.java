package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;

@JacksonXmlRootElement(localName = "Album")
public class Album implements Serializable {

    private static final long serialVersionUID = -2978497906484121517L;

    /**
     * Название альбома.
     */
    private String name;

    /**
     * Год выпуска альбома.
     */
    private long year;

    /**
     * Цена альбома.
     */
    private double price;

    /**
     * @param nameAlbum
     * @param yearAlbum
     * @param priceAlbum
     */
    public Album(final String nameAlbum,
                 final long yearAlbum,
                 final double priceAlbum) {
        this.name = nameAlbum;
        this.year = yearAlbum;
        this.price = priceAlbum;
    }

    /**
     *
     */
    public Album() {
    }

    /**
     * @return name
     */
    @JacksonXmlProperty(isAttribute = true)
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * @return year
     */
    @JacksonXmlProperty(isAttribute = true)
    @JsonProperty("year")
    public long getYear() {
        return year;
    }

    /**
     * @return price
     */
    @JacksonXmlProperty(isAttribute = true)
    @JsonProperty("price")
    public double getPrice() {
        return price;
    }

}
