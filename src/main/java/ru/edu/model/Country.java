package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "Country")
public class Country implements Serializable {

    private static final long serialVersionUID = -1417339122165691438L;
    /**
     * Имя страны.
     */
    private String name;

    /**
     * Список артистов.
     */
    private List<Artist> artists = new ArrayList<>();

    /**
     *
     */
    public Country() {
    }

    /**
     * @param nameCountry
     */
    public Country(final String nameCountry) {
        this.name = nameCountry;
    }

    /**
     * @param listArtists
     */
    public void setArtists(final List<Artist> listArtists) {
        this.artists = listArtists;
    }

    /**
     * Добавление артистов.
     *
     * @param artist
     */
    public void addArtist(final Artist artist) {
        artists.add(artist);
    }

    /**
     * @return List<Artist>
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("Artist")
    public List<Artist> getArtists() {
        return artists;
    }

    /**
     * @return name
     */
    @JacksonXmlProperty(isAttribute = true)
    @JsonProperty("Name")
    public String getName() {
        return name;
    }

}
