package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CD {

    /**
     * Наименование альбома.
     */
    private String title;

    /**
     * Имя исполнителя.
     */
    private String artist;

    /**
     * Страна исполнителя.
     */
    private String country;

    /**
     * Компания.
     */
    private String company;

    /**
     * Цена.
     */
    private double price;

    /**
     * Год выпуска альбома.
     */
    private int year;

    /**
     * @return title
     */
    @JsonProperty("TITLE")
    public String getTitle() {
        return title;
    }

    /**
     * @return artist
     */
    @JsonProperty("ARTIST")
    public String getArtist() {
        return artist;
    }

    /**
     * @return country
     */
    @JsonProperty("COUNTRY")
    public String getCountry() {
        return country;
    }

    /**
     * @return company
     */
    @JsonProperty("COMPANY")
    public String getCompany() {
        return company;
    }

    /**
     * @return price
     */
    @JsonProperty("PRICE")
    public double getPrice() {
        return price;
    }

    /**
     * @return year
     */
    @JsonProperty("YEAR")
    public int getYear() {
        return year;
    }


    /**
     * @param titleCd
     */
    public void setTitle(final String titleCd) {
        this.title = titleCd;
    }

    /**
     * @param artistCd
     */
    public void setArtist(final String artistCd) {
        this.artist = artistCd;
    }

    /**
     * @param countryCd
     */
    public void setCountry(final String countryCd) {
        this.country = countryCd;
    }

    /**
     * @param companyCd
     */
    public void setCompany(final String companyCd) {
        this.company = companyCd;
    }

    /**
     * @param priceCd
     */
    public void setPrice(final double priceCd) {
        this.price = priceCd;
    }

    /**
     * @param yearCd
     */
    public void setYear(final int yearCd) {
        this.year = yearCd;
    }

}
