package ru.edu.model;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "Artist")
public class Artist implements Serializable {

    private static final long serialVersionUID = -9216394663261198131L;

    /**
     * Имя исполнителя.
     */
    private String name;

    /**
     * Альбомы исполнителя.
     */
    private List<Album> albums = new ArrayList<>();

    /**
     * @param nameArtist
     */
    public Artist(final String nameArtist) {
        this.name = nameArtist;
    }

    /**
     *
     */
    public Artist() {
    }

    /**
     * @param listAlbums
     */
    public void setAlbums(final List<Album> listAlbums) {
        this.albums = listAlbums;
    }

    /**
     * Добавление альбома.
     *
     * @param album
     */
    public void addAlbum(final Album album) {
        albums.add(album);
    }

    /**
     * @return name
     */
    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    /**
     * @return albums
     */
    @JacksonXmlElementWrapper(localName = "Albums")
    @JsonProperty("Album")
    public List<Album> getAlbums() {
        return albums;
    }

}
