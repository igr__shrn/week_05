package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "Catalog")
public class Catalog {

    /**
     * Список CD.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("CD")
    private List<CD> cd = new ArrayList<>();

    /**
     * @return cd
     */
    public List<CD> getCd() {
        return cd;
    }

}
