package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "ArtistRegistry")
public class Registry implements Serializable {

    private static final long serialVersionUID = 1161922932883728606L;
    /**
     * Количество стран в регистре.
     */
    private int countryCount;

    /**
     * Список стран исполнителей.
     */
    private List<Country> countries = new ArrayList<>();


    /**
     * Установка списка стран.
     *
     * @param countriesList
     */
    public void setCountries(final List<Country> countriesList) {
        this.countries = countriesList;
    }

    /**
     * Установка количества стран.
     *
     * @param count
     */
    public void setCountryCount(final int count) {
        this.countryCount = count;
    }

    /**
     * @return countryCount
     */
    @JacksonXmlProperty(isAttribute = true)
    public int getCountryCount() {
        return countryCount;
    }

    /**
     * @return List<Country>
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("Country")
    public List<Country> getCountries() {
        return countries;
    }

}
