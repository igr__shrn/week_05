package ru.edu.app;

import ru.edu.Application;

public final class ConsoleApp {

    /**
     *
     */
    public static final String ARG_INPUT = "-input=";
    /**
     *
     */
    public static final String ARG_OUTPUT = "-output=";
    /**
     *
     */
    public static final String ARG_FORMAT = "-format=";

    /**
     *
     */
    private ConsoleApp() {
    }

    /**
     * В аргументах ожидаем
     * -input=filePath -output=outputFile -format=XML/JSON/JAVA.
     *
     * @param args
     */
    public static void main(final String[] args) {
        String input = "";
        String output = "";
        String type = "";

        for (String arg : args) {
            if (arg.startsWith(ARG_INPUT)) {
                input = arg.substring(ARG_INPUT.length());
            }

            if (arg.startsWith(ARG_OUTPUT)) {
                output = arg.substring(ARG_OUTPUT.length());
            }

            if (arg.startsWith(ARG_FORMAT)) {
                type = arg.substring(ARG_FORMAT.length());
            }

            if (arg.startsWith("-help")) {
                System.out.println(
                        "-input=filePath "
                                +
                                "-output=outputFile -format=XML/JSON/JAVA");
                return;
            }

        }
        Application app = new Application(input, output, type);

    }
}
