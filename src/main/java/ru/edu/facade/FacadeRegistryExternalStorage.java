package ru.edu.facade;

import ru.edu.model.Registry;

/**
 * Попробовал использовать паттерн "фасад".
 */
public class FacadeRegistryExternalStorage {

    /**
     * Путь до файла-источника.
     */
    private String inputPath;

    /**
     * Путь до файла записи.
     */
    private String outputPath;

    /**
     * Формат записи.
     */
    private String outputFormat;

    /**
     * Экземпляр создателя регистра.
     */
    private BuilderRegistry builderRegistry;

    /**
     * Экземпляр для чтения из исходного файла.
     */
    private SourceReaderXml reader;

    /**
     * Экземпляр регистра.
     */
    private RegistryExternalStorage storage;

    /**
     * @param input
     * @param output
     * @param format
     */
    public FacadeRegistryExternalStorage(final String input,
                                         final String output,
                                         final String format) {
        inputPath = input;
        outputPath = output;
        outputFormat = format;
        builderRegistry = new BuilderRegistry();
        reader = new SourceReaderXml();
        storage = RegistryExternalStorageFactory.factoryExternalStorage(format);
    }

    /**
     * Запускает процесс чтения, преобразования и записи в файл.
     */
    public void go() {
        reader.setup(inputPath);
        Registry registry = reader.readSource(builderRegistry);
        saveFile(registry);
    }

    /**
     * Сохраняет регистр в файл.
     *
     * @param registry
     */
    private void saveFile(final Registry registry) {
        storage.setup(outputPath);
        storage.save(registry);
    }

}
