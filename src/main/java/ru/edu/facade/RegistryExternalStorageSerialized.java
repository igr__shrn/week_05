package ru.edu.facade;

import ru.edu.model.Registry;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class RegistryExternalStorageSerialized
        implements RegistryExternalStorage {

    /**
     * Тип формата записи.
     */
    private final String type = "JAVA";

    /**
     * Файл для записи.
     */
    private File file;

    /**
     * Установка файла для записи.
     *
     * @param source
     */
    @Override
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException();
        }
        file = new File(source);
        if (!file.exists()) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Записывает сериализованный регистр в файл.
     *
     * @param registry
     */
    @Override
    public void save(final Registry registry) {
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream =
                    new ObjectOutputStream(outputStream);

            objectOutputStream.writeObject(registry);
            objectOutputStream.close();
        } catch (IOException e) {
            try {
                throw new IOException(e.getMessage());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * @return тип класса
     */
    @Override
    public String getType() {
        return type;
    }
}
