package ru.edu.facade;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.edu.model.Registry;

import java.io.File;
import java.io.IOException;

public class RegistryExternalStorageXml implements RegistryExternalStorage {

    /**
     * Тип формата записи.
     */
    private final String type = "XML";

    /**
     * Файл для записи.
     */
    private File file;

    /**
     * Путь до файла записи.
     *
     * @param source
     */
    @Override
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException();
        }
        file = new File(source);
        if (!file.exists()) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Записывает регистр в файл.
     *
     * @param registry
     */
    @Override
    public void save(final Registry registry) {
        try {
            XmlMapper xmlMapper = new XmlMapper();
            xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
            xmlMapper.writeValue(file, registry);
        } catch (IOException e) {
            try {
                throw new IOException(e.getMessage());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * @return тип класса
     */
    @Override
    public String getType() {
        return type;
    }
}
