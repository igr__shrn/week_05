package ru.edu.facade;

public final class RegistryExternalStorageFactory {

    /**
     *
     */
    private RegistryExternalStorageFactory() {
    }

    /**
     * Статичный метод. Возвращает экземпляр для записи регистра,
     * в зависимости от переданного формата.
     *
     * @param formatOutput
     * @return RegistryExternalStorage
     */
    public static RegistryExternalStorage factoryExternalStorage(
            final String formatOutput) {
        switch (formatOutput) {
            case "XML":
                return new RegistryExternalStorageXml();
            case "JSON":
                return new RegistryExternalStorageJson();
            case "JAVA":
                return new RegistryExternalStorageSerialized();
            default:
                throw new IllegalArgumentException("Формат"
                        + formatOutput
                        + " не поддерживается.\n"
                + "Ожидаем XML/JSON/JAVA");
        }
    }
}
