package ru.edu.facade;

import ru.edu.model.Album;
import ru.edu.model.Artist;
import ru.edu.model.Country;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;


public class BuilderRegistry {

    /**
     * Экземпляр регистра.
     */
    private Registry registry;

    /**
     * Результирующий список стран.
     */
    private List<Country> countries;

    /**
     * Конструктор.
     */
    public BuilderRegistry() {
        registry = new Registry();
        countries = new ArrayList<>();
    }

    /**
     * @param catalog
     * @return регистр
     */
    public Registry build(final Catalog catalog) {
        Map<Country, Map<Artist, List<Album>>> countryMap = catalog.getCd()
                .stream()
                .collect(groupingBy(cd -> new Country(cd.getCountry()),
                                groupingBy(cd -> new Artist(cd.getArtist()),
                                        mapping(cd ->
                                                new Album(cd.getTitle(),
                                                        cd.getYear(),
                                                        cd.getPrice()), toList()
                                        )
                                )
                        )
                );

        getCountries(countryMap);
        buildRegistry();

        return registry;
    }

    private void buildRegistry() {
        registry.setCountryCount(countries.size());
        registry.setCountries(countries);
    }

    private void getCountries(final Map<Country, Map<Artist, List<Album>>>
                                      countryMap) {
        for (Map.Entry<Country, Map<Artist, List<Album>>> entry
                : countryMap.entrySet()) {
            Country tmpCountry = getCountry(entry);
            countries.add(tmpCountry);
        }
    }

    private Country getCountry(
            final Map.Entry<Country, Map<Artist, List<Album>>> entry) {
        Country country = entry.getKey();
        for (Map.Entry<Artist, List<Album>> entryNext
                : entry.getValue().entrySet()) {
            country.addArtist(getArtistWithAlbums(entryNext));
        }

        return country;
    }

    private Artist getArtistWithAlbums(
            final Map.Entry<Artist, List<Album>> entryNext) {
        Artist tmpArtist = entryNext.getKey();
        entryNext.getKey().setAlbums(entryNext.getValue());

        return tmpArtist;
    }

}
