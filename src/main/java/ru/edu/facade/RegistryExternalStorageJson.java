package ru.edu.facade;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import ru.edu.model.Registry;

import java.io.File;
import java.io.IOException;

public class RegistryExternalStorageJson implements RegistryExternalStorage {

    /**
     * Тип формата записи.
     */
    private final String type = "JSON";

    /**
     * Файл для записи.
     */
    private File file;

    /**
     * Установка файла для записи.
     *
     * @param source
     */
    @Override
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException();
        }
        file = new File(source);
        if (!file.exists()) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Записывает регистр в файл.
     *
     * @param registry
     */
    @Override
    public void save(final Registry registry) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            mapper.writeValue(file, registry);

        } catch (IOException e) {
            try {
                throw new IOException(e.getMessage());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * @return тип класса
     */
    @Override
    public String getType() {
        return type;
    }

}
