package ru.edu.facade;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.edu.model.Catalog;
import ru.edu.model.Registry;

import java.io.File;
import java.io.IOException;

public class SourceReaderXml {

    /**
     * Файл-источник.
     */
    private File file;

    /**
     * Установка файла-источника.
     *
     * @param source
     */
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException();
        }
        file = new File(source);
        if (!file.exists()) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Читает файл-источник и возвращает экземпляр Catalog.
     *
     * @param builderRegistry
     * @return Catalog
     */
    public Registry readSource(final BuilderRegistry builderRegistry) {
        Catalog catalog = null;
        try {
            XmlMapper xmlMapper = new XmlMapper();
            catalog = xmlMapper.readValue(file, Catalog.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builderRegistry.build(catalog);
    }
}
