package ru.edu.facade;

import ru.edu.model.Registry;

public interface RegistryExternalStorage {

    /**
     * Путь до файла записи.
     *
     * @param source
     */
    void setup(String source);

    /**
     * Записывает регистр в файл.
     *
     * @param registry
     */
    void save(Registry registry);

    /**
     *
     * @return тип класса
     */
    String getType();
}
