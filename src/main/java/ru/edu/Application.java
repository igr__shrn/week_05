package ru.edu;

import ru.edu.facade.FacadeRegistryExternalStorage;

/**
 * Класс с логикой чтения исходного XML,
 * преобразованием и записью реестра в выбранный тип.
 */

public class Application {
    /**
     * @param input
     * @param output
     * @param type
     */
    public Application(final String input,
                       final String output,
                       final String type) {
        System.out.println(input);
        System.out.println(output);
        System.out.println(type);

        FacadeRegistryExternalStorage storage =
                new FacadeRegistryExternalStorage(input, output, type);
        storage.go();
    }
    //java -jar week_05.jar
    //-input=C:\Java\Projects\week_05\input\cd_catalog.xml
    //-output=C:\Java\Projects\week_05\output\artist.json -format=JSON

}
